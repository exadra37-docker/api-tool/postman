# DOCKER API TOOL POSTMAN

Runs Postman from a Docker Container.

## MENU

* **The Package**
    + [Why Exists?](https://gitlab.com/exadra37-docker/api-tool/postman/blob/master/docs/the-package/why_exists.md)
    + [What Is It?](https://gitlab.com/exadra37-docker/api-tool/postman/blob/master/docs/the-package/what_is_it.md)
    + [When To use It?](https://gitlab.com/exadra37-docker/api-tool/postman/blob/master/docs/the-package/when_to_use_it.md)
* **How To**
    + [Install](https://gitlab.com/exadra37-docker/api-tool/postman/blob/master/docs/how-to/install.md)
    + [Use](https://gitlab.com/exadra37-docker/api-tool/postman/blob/master/docs/how-to/use.md)
    + [Report an Issue](https://gitlab.com/exadra37-docker/api-tool/postman/blob/master/docs/how-to/create_an_issue.md)
    + [Create a Branch](https://gitlab.com/exadra37-docker/api-tool/postman/blob/master/docs/how-to/create_branches.md)
    + [Open a Merge Request](https://gitlab.com/exadra37-docker/api-tool/postman/blob/master/docs/how-to/create_a_merge_request.md)
    + [Uninstall](https://gitlab.com/exadra37-docker/api-tool/postman/blob/master/docs/how-to/uninstall.md)
* **Demos**
    + [Api Demo](https://gitlab.com/exadra37-docker/api-tool/postman/blob/master/docs/demos/api-demo.md)
* **Road Map**
    + [Milestones](https://gitlab.com/exadra37-docker/api-tool/postman/milestones)
    + [Overview](https://gitlab.com/exadra37-docker/api-tool/postman/boards)
* **About**
    + [Author](https://gitlab.com/exadra37-docker/api-tool/postman/blob/master/AUTHOR.md)
    + [Contributors](https://gitlab.com/exadra37-docker/api-tool/postman/blob/master/CONTRIBUTORS.md)
    + [Contributing](https://gitlab.com/exadra37-docker/api-tool/postman/blob/master/CONTRIBUTING.md)
    + [License](https://gitlab.com/exadra37-docker/api-tool/postman/blob/master/LICENSE)


## SUPPORT DEVELOPMENT

If this is useful for you, please:

* Share it on [Twitter](https://twitter.com/home?status=https%3A//github.com/exadra37-versioning/explicit-versioning%20a%20%23versioning%20specification%20for%20%23developers%20that%20care%20about%20release%20%23software%20with%20explicit%20breaking%20changes.%23semver) or in any other channel of your preference.
* Consider to [offer me](https://www.paypal.me/exadra37) a coffee, a beer, a dinner or any other treat 😎.


## EXPLICIT VERSIONING

This repository uses [Explicit Versioning](https://gitlab.com/exadra37-versioning/explicit-versioning) schema.


## BRANCHES

Branches are created as demonstrated [here](docs/how-to/create_branches.md).

This are the type of branches we can see at any moment in the repository:

* `master` - Issues and milestones branches will be merged here. Don't use it in
              production.
* `latest` - Matches the last stable tag created. Useful for automation tools.
             Doesn't guarantee backwards compatibility.
* `4-fix-some-bug` - Each issue will have is own branch for development.
* `milestone-12_add-some-new-feature` - all Milestone issues will start, tracked and merged
                             here.

Only `master` and `latest` branches will be permanent ones in the repository and
all other ones will be removed once they are merged.


## DISCLAIMER

I code for passion and when coding I like to do as it pleases me...

You know I do this in my free time, thus I want to have fun and enjoy it ;).

Professionally I will do it as per company guidelines and standards.
